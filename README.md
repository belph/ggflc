# GGFLC (Gotta Go Fast Line Counter)

![Logo](/img/logo.png)

A half-joke-half-actually-useful-tool, this repository implements a line counter particularly 
optimized for larger files, which utilizes a combination of `mmap` and SSE instructions.

Credit to [Neil Locketz][nl] for `SSE` suggestion.

[nl]: https://github.com/c0d3d
